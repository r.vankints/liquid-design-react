# @liquid-design/liquid-design-react

> Liquid Design System components for React. Design System and Component Kit to create beautiful applications.

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com) [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/) [![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

## Documentation

You can find it [here](http://docs.merck.design).

## Contributing

see [CONTRIBUTING.md](CONTRIBUTING.md)
